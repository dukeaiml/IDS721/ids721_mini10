# IDS 721 Mini 10

### Dependencies


`pytorch`

`transformers`

`numpy`

Install the necessary libraries:


```bash
pip install --no-cache -r requirements.txt

```


### App

Create the llm app:


```python
import json
import torch
import tempfile
from transformers import pipeline
import os

model = pipeline("text-generation", model="openai-gpt")


tempfile.tempdir = '/tmp'
os.environ['TRANSFORMERS_CACHE'] = '/tmp'
def lambda_handler(event, context):

    input_text = event.get("input_text", "")


    generated_text = generate_text(input_text)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "generated_text": generated_text
        })
    }

def generate_text(input_text):
    text = input_text
    content = model(text, max_length=100, num_return_sequences=1)[0]['generated_text']
    return content


```

### Dockerize

```bash

# Dockerfile
FROM python:3.9-slim

WORKDIR /app

COPY app.py requirements.txt /app/

RUN pip install --no-cache-dir -r requirements.txt


ENTRYPOINT [ "/usr/local/bin/python", "-m", "awslambdaric" ]

CMD [ "app.lambda_handler" ]

```

### Run 

```bash
docker build -t myapp . 
```

To test the lambda function locally 

```bash
docker run --platform linux/arm64 -d -v ~/.aws-lambda-rie:/aws-lambda -p 9000:8080 \
    --entrypoint /aws-lambda/aws-lambda-rie \
    myapp \
        /usr/local/bin/python -m awslambdaric app.lambda_handler
```

Call the function 


```bash
curl "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{"input_text":"hello world!"}'

```


![Alt text](image.png)

### Deploy


1. Push the image to AWS ECR 

![Alt text](image-1.png)

2. Create aws lambda function from container image

![Alt text](image-2.png)

3. Set up the configuration and permission
