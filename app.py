import json
import torch
import tempfile
from transformers import pipeline
import os

model = pipeline("text-generation", model="openai-gpt")
# 设置临时目录


tempfile.tempdir = '/tmp'
os.environ['TRANSFORMERS_CACHE'] = '/tmp'
def lambda_handler(event, context):
    # 获取输入文本
    input_text = event.get("input_text", "")

    # 生成文本
    generated_text = generate_text(input_text)

    # 返回结果
    return {
        "statusCode": 200,
        "body": json.dumps({
            "generated_text": generated_text
        })
    }

def generate_text(input_text):
    text = input_text
    content = model(text, max_length=100, num_return_sequences=1)[0]['generated_text']
    return content
